## Getting Started

Follow the steps below to set up and run the project:

1. Install dependencies by running:

```bash
$ npm install
```

2. Run the application by running:

```bash
$ npm run dev
```

3. Then, open the application in your browser:

```
http://localhost:5173/
```
