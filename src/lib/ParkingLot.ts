import { CHAR, Coordinates, ParkingLotSize, RATES, Vehicle } from "@/types";

class ParkingLot {
  entryPointsCoords: Coordinates[];
  parkingLotSize: ParkingLotSize;
  parkedVehicles: never[] | Vehicle[];
  unparkedVehicles: never[] | Vehicle[];

  constructor(
    entryPointsCoords: never[] | Coordinates[],
    parkingLotSize: ParkingLotSize
  ) {
    this.entryPointsCoords = entryPointsCoords;
    this.parkingLotSize = parkingLotSize;
    this.parkedVehicles = [];
    this.unparkedVehicles = [];
  }

  /* ENTRY POINTS */

  isEntryPoint(rowIndex: number, colIndex: number) {
    return this.entryPointsCoords.some(
      (coord) => coord.rowIndex === rowIndex && coord.colIndex === colIndex
    );
  }

  addEntryPoint(rowIndex: number, colIndex: number) {
    this.entryPointsCoords = [
      ...this.entryPointsCoords,
      { rowIndex, colIndex },
    ];
    return this.entryPointsCoords;
  }

  removeEntryPoint(rowIndex: number, colIndex: number) {
    const filtered = this.entryPointsCoords.filter(
      (coord) => coord.rowIndex !== rowIndex || coord.colIndex !== colIndex
    );
    this.entryPointsCoords = filtered;
    return this.entryPointsCoords;
  }

  entryPointsLabel(rowIndex: number, colIndex: number) {
    const index = this.entryPointsCoords.findIndex(
      (coord) => coord.rowIndex === rowIndex && coord.colIndex === colIndex
    );
    return CHAR[index];
  }

  /* PARKING LOT */

  getParkingKey(rowIndex: number, colIndex: number) {
    const keys = Object.keys(this.parkingLotSize) as Array<
      keyof ParkingLotSize
    >;
    for (const key of keys) {
      if (
        this.parkingLotSize[key].some(
          (item) => item.rowIndex === rowIndex && item.colIndex === colIndex
        )
      ) {
        return key;
      }
    }
    return null;
  }

  updateParkingLotSize(
    key: keyof ParkingLotSize,
    type: keyof ParkingLotSize | null,
    rowIndex: number,
    colIndex: number
  ) {
    let updatedParkingLotSize: ParkingLotSize;

    const filtered = this.parkingLotSize[key].filter(
      (coord) => coord.rowIndex !== rowIndex || coord.colIndex !== colIndex
    );

    if (type !== null) {
      updatedParkingLotSize = {
        ...this.parkingLotSize,
        [key]: filtered,
        [type]: [...this.parkingLotSize[type], { rowIndex, colIndex }],
      };
    } else {
      updatedParkingLotSize = {
        ...this.parkingLotSize,
        [key]: filtered,
      };
    }

    this.parkingLotSize = updatedParkingLotSize;
    return this.parkingLotSize;
  }

  getPossibleSlots(vehicleType: string) {
    switch (vehicleType) {
      case "S":
        return [
          ...this.parkingLotSize["S"],
          ...this.parkingLotSize["M"],
          ...this.parkingLotSize["L"],
        ];
      case "M":
        return [...this.parkingLotSize["M"], ...this.parkingLotSize["L"]];
      case "L":
        return [...this.parkingLotSize["L"]];
      default:
        return [];
    }
  }

  /* VEHICLES */

  addParkedVehicle(vehicle: Vehicle) {
    this.parkedVehicles = [...this.parkedVehicles, vehicle];
    return this.parkedVehicles;
  }

  removeParkedVehicle(vehicle: Vehicle) {
    this.parkedVehicles = this.parkedVehicles.filter(
      (v) =>
        v.position.rowIndex !== vehicle.position.rowIndex ||
        v.position.colIndex !== vehicle.position.colIndex
    );
    return this.parkedVehicles;
  }

  addUnparkedVehicle(vehicle: Vehicle) {
    this.unparkedVehicles = [...this.unparkedVehicles, vehicle];
    return this.unparkedVehicles;
  }

  removeUnparkedVehicle(vehicle: Vehicle) {
    this.unparkedVehicles = this.unparkedVehicles.filter(
      (v) =>
        v.position.rowIndex !== vehicle.position.rowIndex ||
        v.position.colIndex !== vehicle.position.colIndex
    );
    return this.unparkedVehicles;
  }

  isParkAvailable(rowIndex: number, colIndex: number) {
    return this.parkedVehicles.find(
      (vehicle) =>
        vehicle.position.rowIndex === rowIndex &&
        vehicle.position.colIndex === colIndex
    );
  }

  isReturningVehicle(vehicle: Vehicle) {
    return this.unparkedVehicles.find(
      (v) => v.plateNumber === vehicle.plateNumber
    );
  }

  /* TIME */

  getTimeDifference(entryTime: string, exitTime: string) {
    const entryTimestamp = new Date(entryTime).getTime();
    const exitTimestamp = new Date(exitTime).getTime();
    const millisecondsInHour = 60 * 60 * 1000;
    const timeDiff = Math.round(
      (exitTimestamp - entryTimestamp) / millisecondsInHour
    );
    return timeDiff;
  }

  /* FEE */

  calculateFee(hoursParked: number, toUnparkVehicle: Vehicle) {
    const firstThreeHours = 3;
    const firstTwentyFourHours = 24;
    const fullDayChunk = 5000;

    let fee = 0;

    if (hoursParked <= firstThreeHours) {
      fee += 40;
    } else if (hoursParked < firstTwentyFourHours) {
      const remainderTime = hoursParked - 3;
      fee += 40;

      const remainderFee =
        remainderTime *
        RATES[toUnparkVehicle.vehicleType as keyof typeof RATES];
      fee += remainderFee;
    } else if (hoursParked > firstTwentyFourHours) {
      const day = Math.floor(hoursParked / 24);
      fee += day * fullDayChunk;

      const remainderTime = hoursParked - day * 24;
      const remainderFee =
        remainderTime *
        RATES[toUnparkVehicle.vehicleType as keyof typeof RATES];
      fee += remainderFee;
    }
    return fee;
  }
}

export default ParkingLot;
