import React from "react";

import ParkingLot from "@/lib/ParkingLot";
import { Coordinates, ParkingLotSize } from "@/types";

type Props = {
  step: number;
  rowSize: number;
  colSize: number;
  parkingLot: ParkingLot;
  handleEntryPoints: (rowIndex: number, colIndex: number) => void;
  entryPointsCoords: never[] | Coordinates[];
  entryPointsCount: number;
  handleChangeType: (
    type: keyof ParkingLotSize,
    rowIndex: number,
    colIndex: number
  ) => void;
  parkingSlotType: (
    rowIndex: number,
    colIndex: number
  ) => JSX.Element | undefined;
};

const Table: React.FC<Props> = ({
  step,
  rowSize,
  colSize,
  parkingLot,
  handleEntryPoints,
  entryPointsCoords,
  entryPointsCount,
  handleChangeType,
  parkingSlotType,
}) => {
  return (
    <table>
      <tbody>
        {Array.from({ length: rowSize }, (_, rowIndex) => (
          <tr key={rowIndex}>
            {Array.from({ length: colSize }, (_, colIndex) => (
              <React.Fragment key={`${rowIndex}${colIndex}`}>
                {step === 2 && (
                  <td
                    className={`w-24 h-24 text-center border-2 border-gray-500 ${
                      parkingLot.isEntryPoint(rowIndex, colIndex) &&
                      "bg-red-100"
                    }`}
                    onClick={() => handleEntryPoints(rowIndex, colIndex)}
                  >
                    {!parkingLot.isEntryPoint(rowIndex, colIndex) &&
                      entryPointsCoords.length === entryPointsCount && (
                        <select
                          className="py-1 px-2 rounded-md"
                          onChange={(e) =>
                            handleChangeType(
                              e.target.value as keyof ParkingLotSize,
                              rowIndex,
                              colIndex
                            )
                          }
                        >
                          <option value="S">S</option>
                          <option value="M">M</option>
                          <option value="L">L</option>
                        </select>
                      )}
                  </td>
                )}
                {step === 3 && (
                  <td
                    className={`w-24 h-24 text-center border-2 border-gray-500 ${
                      parkingLot?.isEntryPoint(rowIndex, colIndex) &&
                      "bg-red-100"
                    }`}
                  >
                    {parkingSlotType(rowIndex, colIndex)}
                  </td>
                )}
              </React.Fragment>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
