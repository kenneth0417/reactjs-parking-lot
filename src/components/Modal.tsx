import React, { Dispatch, SetStateAction, useCallback, useState } from "react";

import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Button } from "@/components/ui/button";
import { Vehicle } from "@/types";
import ParkingLot from "@/lib/ParkingLot";

type Props = {
  toUnparkVehicle: Vehicle;
  handleUnpark: (vehicle: Vehicle) => void;
  setShowUnparkModal: Dispatch<SetStateAction<boolean>>;
  parkingLot: ParkingLot;
};

const Modal: React.FC<Props> = ({
  handleUnpark,
  toUnparkVehicle,
  setShowUnparkModal,
  parkingLot,
}) => {
  const [exitTime, setExitTime] = useState<string | undefined>(undefined);
  const [fee, setFee] = useState<number | undefined>();
  const [error, setError] = useState<boolean>(false);

  const calculateFee = useCallback(() => {
    if (!exitTime) {
      setError(true);
      return;
    }
    const entry = new Date(toUnparkVehicle.entryTime);
    const exit = new Date(exitTime);
    if (entry > exit) {
      return;
    }
    let previousFee = 0;
    const returningVehicle = parkingLot.isReturningVehicle(toUnparkVehicle);

    if (
      returningVehicle &&
      returningVehicle.exitTime &&
      parkingLot.getTimeDifference(
        returningVehicle.exitTime,
        toUnparkVehicle.entryTime
      ) <= 1
    ) {
      const previousHoursParked = parkingLot.getTimeDifference(
        returningVehicle.entryTime,
        returningVehicle.exitTime
      );
      previousFee = parkingLot.calculateFee(
        previousHoursParked,
        returningVehicle
      );
      toUnparkVehicle.entryTime = returningVehicle.entryTime;
    }

    const hoursParked = parkingLot.getTimeDifference(
      toUnparkVehicle.entryTime,
      exitTime
    );
    const fee = parkingLot.calculateFee(hoursParked, toUnparkVehicle);
    setFee(fee - previousFee);
    setError(false);
    toUnparkVehicle.exitTime = exitTime;
    handleUnpark(toUnparkVehicle);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [exitTime, toUnparkVehicle]);

  return (
    <div className="absolute backdrop-blur-sm w-full h-full flex justify-center items-center">
      <div className="w-96 h-auto bg-yellow-50 p-5 rounded-xl border-black border-[1px]">
        <div className="text-right">
          <Button
            className="w-8 h-8 rounded-full border-black"
            variant="outline"
            onClick={() => setShowUnparkModal(false)}
          >
            ❌
          </Button>
        </div>
        <div className="my-2">
          <h3></h3>
        </div>
        <div className="my-2">
          <Label htmlFor="vehicleType">Vehicle Type</Label>
          <Input type="text" value={toUnparkVehicle.vehicleType} disabled />
        </div>
        <div className="my-2">
          <Label htmlFor="plateNumber">Plate Number</Label>
          <Input type="text" value={toUnparkVehicle.plateNumber} disabled />
        </div>
        <div className="my-2">
          <Label htmlFor="entryTime">Entry Time</Label>
          <Input
            type="datetime-local"
            value={toUnparkVehicle.entryTime}
            disabled
          />
        </div>
        <div className="my-2">
          <Label htmlFor="exitTime">Exit Time</Label>
          {error && (
            <h6 className="text-sm text-red-600">
              Please fill out this field.
            </h6>
          )}
          <Input
            type="datetime-local"
            onChange={(e) => setExitTime(e.target.value)}
          />
        </div>
        {fee && (
          <div className="border-[0.5px] border-slate-400 py-1 text-center rounded-md">
            <h1 className="scroll-m-20 text-lg tracking-tight">
              Parking Fee:{" "}
              <span className="text-lime-500 font-semibold">₱{fee}.00</span>
            </h1>
          </div>
        )}
        <Button
          className="mt-5 w-full"
          variant="outline"
          onClick={calculateFee}
        >
          SUBMIT
        </Button>
      </div>
    </div>
  );
};

export default Modal;
