export type Coordinates = {
  rowIndex: number;
  colIndex: number;
};

export type ParkingLotSize = {
  S: never[] | Coordinates[];
  M: never[] | Coordinates[];
  L: never[] | Coordinates[];
};

export type ParkInputs = {
  entryPoint: string;
  vehicleType: string;
  entryTime: string;
  plateNumber: string;
};

export type Vehicle = {
  position: Coordinates;
  entryTime: string;
  exitTime: string | undefined;
  plateNumber: string;
  vehicleType: string;
};

export const RATES = {
  S: 20,
  M: 60,
  L: 100,
};

export const CHAR: Record<number, string> = {
  0: "A",
  1: "B",
  2: "C",
  3: "D",
  4: "E",
  5: "F",
  6: "G",
  7: "H",
  8: "I",
  9: "J",
};
