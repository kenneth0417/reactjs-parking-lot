import { useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import Modal from "./components/Modal";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import Table from "./components/Table";
import { useToast } from "@/components/ui/use-toast";
import {
  CHAR,
  Coordinates,
  ParkInputs,
  ParkingLotSize,
  Vehicle,
} from "./types";
import ParkingLot from "./lib/ParkingLot";

function App() {
  // parking lot size
  const [rowSize, setRowSize] = useState<number>(3);
  const [colSize, setColSize] = useState<number>(3);
  // parking lot utils/functions
  const [parkingLot, setParkingLot] = useState<ParkingLot | undefined>(
    undefined
  );
  // steps
  const [step, setStep] = useState<number>(1);
  // entry points
  const [entryPointsCount, setEntryPointsCount] = useState<number>(3);
  const [entryPointsCoords, setEntryPointsCoords] = useState<
    never[] | Coordinates[]
  >([]);
  const [entryPointsError, setEntryPointsError] = useState<boolean | undefined>(
    undefined
  );
  // parking lot size/type
  const [parkingLotSize, setParkingLotSize] = useState<ParkingLotSize>({
    S: [],
    M: [],
    L: [],
  });
  // vehicles
  const [parkedVehicles, setParkedVehicles] = useState<never[] | Vehicle[]>([]);
  const [unparkedVehicles, setUnparkedVehicles] = useState<never[] | Vehicle[]>(
    []
  );
  // modal
  const [showUnparkModal, setShowUnparkModal] = useState<boolean>(false);
  //selector
  const [toUnparkVehicle, setToUnparkVehicle] = useState<undefined | Vehicle>(
    undefined
  );
  // toast
  const { toast } = useToast();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ParkInputs>();

  const parkingSlotType = (rowIndex: number, colIndex: number) => {
    if (!parkingLot) {
      return;
    }
    const existing = parkingLot.isParkAvailable(rowIndex, colIndex);
    if (existing) {
      return (
        <>
          <h6 className="text-sm font-semibold tracking-tight mb-2">
            {existing.plateNumber}
          </h6>
          <Button
            variant="outline"
            size="sm"
            onClick={() => {
              setToUnparkVehicle(existing);
              setShowUnparkModal(true);
            }}
          >
            OUT
          </Button>
        </>
      );
    }

    const parkingKey = parkingLot.getParkingKey(rowIndex, colIndex);
    if (parkingKey) {
      return (
        <h6 className="text-sm font-semibold tracking-tight">{parkingKey}</h6>
      );
    }

    const index = parkingLot.entryPointsLabel(rowIndex, colIndex);
    if (index) {
      return <h6 className="text-sm font-semibold tracking-tight">{index}</h6>;
    }
  };

  const handleEntryPoints = (rowIndex: number, colIndex: number) => {
    if (!parkingLot || entryPointsCoords.length === entryPointsCount) {
      return;
    }
    const isEntryPointValid =
      rowIndex === 0 ||
      colIndex === 0 ||
      rowIndex === rowSize - 1 ||
      colIndex === colSize - 1;
    if (!isEntryPointValid) {
      return;
    }
    const updatedParking = parkingLot.addEntryPoint(rowIndex, colIndex);
    setEntryPointsCoords(updatedParking);

    const parkingKey = parkingLot.getParkingKey(rowIndex, colIndex);
    if (!parkingKey) {
      return;
    }

    const updatedState = parkingLot.updateParkingLotSize(
      parkingKey,
      null,
      rowIndex,
      colIndex
    );
    setParkingLotSize(updatedState);
  };

  const handleChangeType = (
    type: keyof ParkingLotSize,
    rowIndex: number,
    colIndex: number
  ) => {
    if (!parkingLot) {
      return;
    }
    const parkingKey = parkingLot.getParkingKey(rowIndex, colIndex);
    if (!parkingKey) {
      return;
    }
    const updatedState = parkingLot.updateParkingLotSize(
      parkingKey,
      type,
      rowIndex,
      colIndex
    );
    setParkingLotSize(updatedState);
  };

  const handleUnpark = (vehicle: Vehicle) => {
    if (!parkingLot) {
      return;
    }
    const removeParkedVehicle = parkingLot.removeParkedVehicle(vehicle);
    setParkedVehicles(removeParkedVehicle);

    const unparkedVehicles = parkingLot.addUnparkedVehicle(vehicle);
    setUnparkedVehicles(unparkedVehicles);
  };

  const handlePark: SubmitHandler<ParkInputs> = (data) => {
    if (!parkingLot) {
      return;
    }
    const entry =
      entryPointsCoords[
        data.entryPoint === "A" ? 0 : data.entryPoint === "B" ? 1 : 2
      ];

    const possibleSlots = parkingLot.getPossibleSlots(data.vehicleType);

    let parkingSlot: Coordinates | null = null;

    let nearestDistance = rowSize + colSize - 1;

    for (let coordinates of possibleSlots) {
      const distance =
        Math.abs(entry.rowIndex - coordinates.rowIndex) +
        Math.abs(entry.colIndex - coordinates.colIndex);

      const isAvailable = parkingLot.isParkAvailable(
        coordinates.rowIndex,
        coordinates.colIndex
      );

      if (distance < nearestDistance && !isAvailable) {
        nearestDistance = distance;
        parkingSlot = coordinates;
      }
    }

    if (!parkingSlot) {
      toast({
        title: "Parking Unsuccessful!",
        description: "No available parking slots.",
        variant: "destructive",
      });
      return;
    }

    const vehicle: Vehicle = {
      entryTime: data.entryTime,
      plateNumber: data.plateNumber,
      vehicleType: data.vehicleType,
      position: {
        rowIndex: parkingSlot.rowIndex,
        colIndex: parkingSlot.colIndex,
      },
      exitTime: undefined,
    };

    const updatedParkedVehicles = parkingLot.addParkedVehicle(vehicle);
    setParkedVehicles(updatedParkedVehicles);

    toast({
      title: "Parking Successful!",
      description: `Vehicle assigned at slot (${vehicle.position.rowIndex},${vehicle.position.colIndex}).`,
    });
  };

  const handleChangeSize = (
    e: React.ChangeEvent<HTMLInputElement>,
    type: string
  ) => {
    if (Number(e.target.value) <= 2 || Number(e.target.value) > 10) {
      return;
    }
    if (type === "row") {
      setRowSize(Number(e.target.value));
    } else if (type === "column") {
      setColSize(Number(e.target.value));
    }
  };

  useEffect(() => {
    const allSlots = [];
    for (let rowIndex = 0; rowIndex < rowSize; rowIndex++) {
      for (let colIndex = 0; colIndex < colSize; colIndex++) {
        allSlots.push({ rowIndex, colIndex });
      }
    }
    // set all slots initially to small type
    setParkingLotSize({ S: allSlots, M: [], L: [] });
    setEntryPointsCoords([]);
  }, [rowSize, colSize]);

  useEffect(() => {
    setParkingLot(new ParkingLot(entryPointsCoords, parkingLotSize));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parkingLotSize]);

  return (
    <div className="flex flex-col justify-center items-center min-h-screen relative">
      <h1 className="scroll-m-20 text-4xl font-extrabold tracking-tight lg:text-5xl mb-16">
        00 Parking Lot
      </h1>

      {step === 1 && (
        <>
          <div className="flex items-center">
            <h3 className="scroll-m-20 text-2xl font-semibold tracking-tight">
              Number of Entry Points:{" "}
            </h3>
            <div className="ml-3 flex items-center">
              <Button
                className="text-xl w-14 h-10"
                variant="outline"
                onClick={() => setEntryPointsCount((prev) => prev - 1)}
                disabled={entryPointsCount <= 3}
              >
                -
              </Button>
              <h3 className="mx-4 text-xl">{entryPointsCount}</h3>
              <Button
                className="text-xl w-14 h-10"
                variant="outline"
                onClick={() => setEntryPointsCount((prev) => prev + 1)}
              >
                +
              </Button>
            </div>
          </div>
          <div className="mt-6">
            <Button
              className="text-base w-24 h-10"
              variant="outline"
              onClick={() => setStep((prev) => prev + 1)}
            >
              NEXT
            </Button>
          </div>
        </>
      )}

      {step === 2 && (
        <div>
          <h3 className="scroll-m-20 text-2xl font-semibold tracking-tight">
            Set Parking Lot Map:
          </h3>
          <p className="leading-7 ">
            Please select the cells you want to be your entry point and specify
            each cell's size/type.
          </p>
          {entryPointsError && (
            <h6 className="text-sm text-red-600">
              Please select {entryPointsCount} entry points in the table below.
            </h6>
          )}
          <div className="flex justify-center my-4">
            {parkingLot && (
              <Table
                step={step}
                rowSize={rowSize}
                colSize={colSize}
                parkingLot={parkingLot}
                handleEntryPoints={handleEntryPoints}
                entryPointsCoords={entryPointsCoords}
                entryPointsCount={entryPointsCount}
                handleChangeType={handleChangeType}
                parkingSlotType={parkingSlotType}
              />
            )}
          </div>
          <div className="flex flex-col items-center">
            <div className="flex mb-5">
              <div className="mx-1">
                <Label htmlFor="row">Row</Label>
                <Input
                  type="number"
                  id="row"
                  value={rowSize}
                  onChange={(e) => handleChangeSize(e, "row")}
                />
              </div>
              <div className="mx-1">
                <Label htmlFor="column">Column</Label>
                <Input
                  type="number"
                  id="column"
                  value={colSize}
                  onChange={(e) => handleChangeSize(e, "column")}
                />
              </div>
            </div>
            <Button
              className="text-base w-24 h-10"
              variant="outline"
              onClick={() => {
                if (entryPointsCoords.length !== entryPointsCount) {
                  setEntryPointsError(true);
                  return;
                }
                setEntryPointsError(false);
                setStep((prev) => prev + 1);
              }}
            >
              NEXT
            </Button>
          </div>
        </div>
      )}

      {step === 3 && (
        <div>
          <div className="flex justify-center mb-4">
            {parkingLot && (
              <Table
                step={step}
                rowSize={rowSize}
                colSize={colSize}
                parkingLot={parkingLot}
                handleEntryPoints={handleEntryPoints}
                entryPointsCoords={entryPointsCoords}
                entryPointsCount={entryPointsCount}
                handleChangeType={handleChangeType}
                parkingSlotType={parkingSlotType}
              />
            )}
          </div>
          <h3 className="scroll-m-20 text-2xl font-semibold tracking-tight">
            Parking System:
          </h3>
          <p className="leading-7 ">
            Please fill up the form below to park a car.
          </p>
          <div className="my-2">
            <Label htmlFor="entryPoint">Entry Point: </Label>
            <select
              className="px-2 py-0.5 rounded-md"
              {...register("entryPoint", { required: true })}
            >
              {Array.from({ length: entryPointsCount }, (_, idx) => (
                <option key={idx} value={CHAR[idx]}>
                  {CHAR[idx]}
                </option>
              ))}
            </select>
          </div>
          <div className="my-2">
            <Label htmlFor="vehicleType">Vehicle Type: </Label>
            <select
              className="px-2 py-0.5 rounded-md"
              {...register("vehicleType", { required: true })}
            >
              <option value="S">S</option>
              <option value="M">M</option>
              <option value="L">L</option>
            </select>
          </div>
          <div className="my-2">
            <Label htmlFor="entryTime">Entry Time: </Label>
            {errors.entryTime && (
              <h6 className="text-sm text-red-600">
                Please fill out this field.
              </h6>
            )}
            <Input
              type="datetime-local"
              {...register("entryTime", { required: true })}
            />
          </div>
          <div className="my-2">
            <Label htmlFor="plateNumber">Plate Number: </Label>
            {errors.plateNumber && (
              <h6 className="text-sm text-red-600">
                Please fill out this field.
              </h6>
            )}
            <Input
              type="text"
              placeholder="Plate Number"
              {...register("plateNumber", { required: true })}
            />
          </div>
          <div className="my-2 text-center">
            <Button
              className="text-base w-24 h-10"
              variant="outline"
              onClick={handleSubmit(handlePark)}
            >
              SUBMIT
            </Button>
          </div>
        </div>
      )}

      {showUnparkModal && toUnparkVehicle && parkingLot && (
        <Modal
          toUnparkVehicle={toUnparkVehicle}
          handleUnpark={handleUnpark}
          setShowUnparkModal={setShowUnparkModal}
          parkingLot={parkingLot}
        />
      )}
    </div>
  );
}

export default App;
